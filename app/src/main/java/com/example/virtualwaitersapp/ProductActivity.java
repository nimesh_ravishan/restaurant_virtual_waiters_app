package com.example.virtualwaitersapp;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.widget.Toast;

import com.example.virtualwaitersapp.Adapter.ProductAdapter;
import com.example.virtualwaitersapp.Adapter.ProductCategoryAdapter;
import com.example.virtualwaitersapp.Adapter.ProductSubCategoryAdapter;
import com.example.virtualwaitersapp.Model.Category;
import com.example.virtualwaitersapp.Model.Product;
import com.example.virtualwaitersapp.Model.SubCategory;
import com.example.virtualwaitersapp.OnClick.CategoryClick;
import com.example.virtualwaitersapp.OnClick.ProductClick;
import com.example.virtualwaitersapp.OnClick.SubCategoryClick;
import com.example.virtualwaitersapp.Remote.RetrofitClient;
import com.example.virtualwaitersapp.Service.IpService;
import com.example.virtualwaitersapp.Utills.Utills;
import com.example.virtualwaitersapp.database.DBUtils;
import com.mikepenz.materialdrawer.AccountHeader;
import com.mikepenz.materialdrawer.Drawer;

import java.util.ArrayList;

public class ProductActivity extends AppCompatActivity implements CategoryClick,ProductClick,SubCategoryClick{

    Context mContext;
    Activity mActivity;
    RecyclerView categoryRecycle, subCategoryRecycle,productRecycle;
    ProductCategoryAdapter productCategoryAdapter;
    ProductSubCategoryAdapter productSubCategoryAdapter;
    ProductAdapter productAdapter;
    CategoryClick categoryClick;
    SubCategoryClick subCategoryClick;
    ProductClick productClick;

    IpService mService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product);

        Toolbar productToolbar = findViewById(R.id.toolbar);
        setSupportActionBar(productToolbar);
        setTitle("Products");

        mActivity = ProductActivity.this;
        mContext = ProductActivity.this;

        categoryClick = ProductActivity.this;
        subCategoryClick = ProductActivity.this;
        productClick = ProductActivity.this;

        categoryRecycle = findViewById(R.id.category_recycler_view);
        subCategoryRecycle = findViewById(R.id.subcategory_recycler_view);
        productRecycle = findViewById(R.id.category_grid_view);

        mService = new RetrofitClient().getClient(this).create(IpService.class);

        AccountHeader headerResult = Utills.buildHeader(true, savedInstanceState, mActivity, mActivity);
        Drawer result = Utills.drawerFront1(productToolbar, savedInstanceState, mActivity, mActivity);

        getCategory();



    }
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public void getCategory() {
        ArrayList<Category> categoryArrayList = DBUtils.getCategory(mContext);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        categoryRecycle.setLayoutManager(layoutManager);

        productCategoryAdapter = new ProductCategoryAdapter(mContext, categoryArrayList, categoryClick);
        categoryRecycle.setAdapter(productCategoryAdapter);
    }

    @Override
    public void onItemClick(int position, String categoryId) {
        if (!categoryId.equalsIgnoreCase("")) {
            ArrayList<SubCategory> subCategories = DBUtils.getSubcategoryBYCategoryId(mContext, categoryId);
            if (subCategories.size() != 0) {
                LinearLayoutManager layoutManager = new LinearLayoutManager(this);
                subCategoryRecycle.setLayoutManager(layoutManager);

                productSubCategoryAdapter = new ProductSubCategoryAdapter(mContext, subCategories, subCategoryClick);
                subCategoryRecycle.setAdapter(productSubCategoryAdapter);

            } else {
                Toast.makeText(mContext, "There is no sub Categories", Toast.LENGTH_SHORT).show();
            }
        }

    }

    @Override
    public void onProductItemClick(int position, String product) {

        Intent intent = new Intent(ProductActivity.this, ProductViewActivity.class);
        intent.putExtra("productId", product);
        startActivity(intent);
    }

    @Override
    public void onSubItemClick(int position, String subCategoryId) {
        if (!subCategoryId.equalsIgnoreCase("")) {
            ArrayList<Product> productArrayList = DBUtils.getProductBYSubCategoryId(mContext, subCategoryId);
            if (productArrayList.size() != 0) {
                productRecycle.setLayoutManager(new GridLayoutManager(this, 2));

                productAdapter = new ProductAdapter(mContext, productArrayList, productClick);
                productRecycle.setAdapter(productAdapter);

            } else {
                Toast.makeText(mContext, "There is no Products", Toast.LENGTH_SHORT).show();
            }
        }

    }
}