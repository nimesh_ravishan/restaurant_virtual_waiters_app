package com.example.virtualwaitersapp.OnClick;

import com.example.virtualwaitersapp.Model.OrderProduct;

import java.util.ArrayList;

public interface OrderClick {
    void onOrderItemClick(int position, ArrayList<OrderProduct> orderProduct);

}
