package com.example.virtualwaitersapp.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.virtualwaitersapp.Model.SubCategory;
import com.example.virtualwaitersapp.OnClick.SubCategoryClick;
import com.example.virtualwaitersapp.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class ProductSubCategoryAdapter extends RecyclerView.Adapter<ProductSubCategoryAdapter.MyViewHolder> {

    private final LayoutInflater inflater;
    Context context;
    ArrayList<SubCategory> subCategoryArrayList;
    SubCategoryClick subCategoryClick;

    public ProductSubCategoryAdapter(Context context, ArrayList<SubCategory> subCategoryArrayList, SubCategoryClick subCategoryClick) {
        this.context = context;
        this.subCategoryArrayList = subCategoryArrayList;
        this.subCategoryClick = subCategoryClick;
        inflater = LayoutInflater.from(context);

    }


    @NonNull
    @Override
    public ProductSubCategoryAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.product_category_list_item, parent, false);
        return new ProductSubCategoryAdapter.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ProductSubCategoryAdapter.MyViewHolder holder, final int position) {

        final SubCategory  subCategory = subCategoryArrayList.get(position);

        if (!subCategory.getSubCategoryName().equalsIgnoreCase("") && subCategory.getSubCategoryName() != null && !subCategory.getSubCategoryName().equalsIgnoreCase("null")) {
            holder.catName.setText(subCategory.getSubCategoryName());
        }
        if (!subCategory.getSubCategoryImage().equalsIgnoreCase("") && subCategory.getSubCategoryImage() != null && !subCategory.getSubCategoryImage().equalsIgnoreCase("null")) {
                holder.catImage.setVisibility(View.GONE);

                Picasso.get().load(subCategory.getSubCategoryImage())
                        .into(holder.catImage, new com.squareup.picasso.Callback() {
                            @Override
                            public void onSuccess() {
                                holder.imgProgress.setVisibility(View.GONE);
                                holder.catImage.setVisibility(View.VISIBLE);
                            }

                            @Override
                            public void onError(Exception e) {
                                holder.catImage.setVisibility(View.VISIBLE);

                            }

                        });


        }else{
            holder.catImage.setImageDrawable(context.getResources().getDrawable(R.drawable.logo));

        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                subCategoryClick.onSubItemClick(position, subCategory.getSubCategoryId());
            }
        });



    }

    @Override
    public int getItemCount() {
        return subCategoryArrayList.size();
    }


    protected class MyViewHolder extends RecyclerView.ViewHolder {

        ImageView catImage;
        TextView catName;
        ProgressBar imgProgress;


        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            catImage = itemView.findViewById(R.id.categoryImage);
            catName = itemView.findViewById(R.id.catName);
            imgProgress = itemView.findViewById(R.id.img_progress);

        }
    }


}
