package com.example.virtualwaitersapp.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.virtualwaitersapp.Model.Category;
import com.example.virtualwaitersapp.OnClick.CategoryClick;
import com.example.virtualwaitersapp.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class ProductCategoryAdapter extends RecyclerView.Adapter<ProductCategoryAdapter.MyViewHolder> {
    private final LayoutInflater inflater;
    ArrayList<Category> categories;
    Context context;
    CategoryClick categoryClick;

    public ProductCategoryAdapter(Context context, ArrayList<Category> categoryArrayList, CategoryClick categoryClick) {
        this.context = context;
        this.categories = categoryArrayList;
        this.categoryClick = categoryClick;
        inflater = LayoutInflater.from(context);

    }

    @NonNull
    @Override
    public ProductCategoryAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.product_category_list_item, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ProductCategoryAdapter.MyViewHolder holder, final int position) {

        final Category category = categories.get(position);

        if (!category.getCategoryName().equalsIgnoreCase("") && category.getCategoryName() != null && !category.getCategoryName().equalsIgnoreCase("null")) {
            holder.catName.setText(category.getCategoryName());
        }
        if (!category.getCategoryImage().equalsIgnoreCase("") && category.getCategoryImage() != null && !category.getCategoryImage().equalsIgnoreCase("null")) {
                holder.catImage.setVisibility(View.GONE);

                Picasso.get().load(category.getCategoryImage())
                        .into(holder.catImage, new com.squareup.picasso.Callback() {
                            @Override
                            public void onSuccess() {
                                holder.imgProgress.setVisibility(View.GONE);
                                holder.catImage.setVisibility(View.VISIBLE);
                            }

                            @Override
                            public void onError(Exception e) {
                                holder.catImage.setVisibility(View.VISIBLE);

                            }

                        });


        }else{
            holder.catImage.setImageDrawable(context.getResources().getDrawable(R.drawable.logo));

        }


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                categoryClick.onItemClick(position, category.getCategoryId());
            }
        });


    }

    @Override
    public int getItemCount() {
        return categories.size();
    }


    protected class MyViewHolder extends RecyclerView.ViewHolder {

        ImageView catImage;
        TextView catName;
        ProgressBar imgProgress;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            catImage = itemView.findViewById(R.id.categoryImage);
            catName = itemView.findViewById(R.id.catName);
            imgProgress = itemView.findViewById(R.id.img_progress);

        }
    }

}
