package com.example.virtualwaitersapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;

import com.example.virtualwaitersapp.Adapter.RatingListAdapter;
import com.example.virtualwaitersapp.Model.Product;
import com.example.virtualwaitersapp.Model.Rating;
import com.example.virtualwaitersapp.Remote.RetrofitClient;
import com.example.virtualwaitersapp.Service.IpService;
import com.example.virtualwaitersapp.Utills.Utills;
import com.example.virtualwaitersapp.database.DBUtils;
import com.google.gson.JsonObject;
import com.mikepenz.materialdrawer.AccountHeader;
import com.mikepenz.materialdrawer.Drawer;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProductViewActivity extends AppCompatActivity {

    Activity mActivity;
    Context mContext;
    ImageView productImage;
    TextView productName, productPrice, productDescription,offer, ratingCount, ratingUserCount;
    ProgressBar imgProgress;
    IpService mService;
    Product product;
    ArrayList<Rating> ratings;
    int usersCount;
    float allRatingCount;
    RecyclerView ratingListItem;
    RatingListAdapter ratingListAdapter;
    RatingBar ratingBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_view);

        productImage = findViewById(R.id.productViewImage);
        productName = findViewById(R.id.productName);
        productPrice = findViewById(R.id.productPrice);
        productDescription = findViewById(R.id.productDescription);
        offer = findViewById(R.id.offer);
        imgProgress = findViewById(R.id.img_progress);
        ratingListItem = findViewById(R.id.rating_list);
        ratingBar = findViewById(R.id.allRatingCount);
        ratingUserCount = findViewById(R.id.ratingUserCount);
        ratingCount = findViewById(R.id.ratingCount);

        Intent intent = getIntent();
        String productId = intent.getStringExtra("productId");

        mActivity = ProductViewActivity.this;
        mContext = this;
        mService = new RetrofitClient().getClient(this).create(IpService.class);

        Toolbar productToolbar = findViewById(R.id.toolbar);
        setSupportActionBar(productToolbar);
        setTitle(DBUtils.getProductNameByProductId(mContext,productId));

        AccountHeader headerResult = Utills.buildHeader(true, savedInstanceState, mActivity, mActivity);
        Drawer result = Utills.drawerFront1(productToolbar, savedInstanceState, mActivity, mActivity);


//        Product product = DBUtils.getProductByProductId(mContext, productId);


        HashMap<String, String> stringHashMap = new HashMap<>();
        stringHashMap.put("product_id", productId);


        mService.getProductById(stringHashMap).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.code() == 200) {
                    try {
                        JSONObject jsonObject = new JSONObject(String.valueOf(response.body()));
                        JSONArray jsonArray = jsonObject.getJSONArray("data");
                        for(int x = 0; x<jsonArray.length(); x++){
                            product = new Product();
                            product.setProductId(String.valueOf(jsonArray.getJSONObject(x).get("product_id")));
                            product.setProductName(String.valueOf(jsonArray.getJSONObject(x).get("product_name")));
                            product.setProductDescription(String.valueOf(jsonArray.getJSONObject(x).get("product_description")));
                            product.setProductPrice(String.valueOf(jsonArray.getJSONObject(x).get("product_price")));
                            product.setProductImage(String.valueOf(jsonArray.getJSONObject(x).get("product_image")));
                            product.setProductCategory(String.valueOf(jsonArray.getJSONObject(x).get("product_category")));
                            product.setProductSubCategory(String.valueOf(jsonArray.getJSONObject(x).get("product_sub_category")));
                            product.setProductDiscountPercentage(String.valueOf(jsonArray.getJSONObject(x).get("product_discount_percentage")));
                            product.setPromotion(String.valueOf(jsonArray.getJSONObject(x).get("promotion")));
                            product.setRatings(String.valueOf(jsonArray.getJSONObject(x).get("ratings")));
                            product.setProductAvailability(Integer.valueOf(String.valueOf(jsonArray.getJSONObject(x).get("product_availability"))));
                        }

                        viewProductDetails(product);




                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    Log.e("TAG", "SOMETHING WENT WRONG!");
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {

            }
        });








/*
        if (!product.getPromotion().equalsIgnoreCase("[]")) {
            try {
                JSONArray jsonArray = new JSONArray(product.getPromotion());
                for (int i = 0; i < jsonArray.length(); i++) {
                    int promotionStatus = Integer.parseInt(jsonArray.getJSONObject(i).getString("activate_promotion"));

                    if (promotionStatus != 0) {
                        offer.setVisibility(View.VISIBLE);
                        offer.setText(jsonArray.getJSONObject(i).getString("offer_percentage") + "%");
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }


        }


        if (!product.getProductImage().equalsIgnoreCase("") && product.getProductImage() != null && !product.getProductImage().equalsIgnoreCase("null")) {
            productImage.setVisibility(View.GONE);

            Picasso.get().load(product.getProductImage())
                    .into(productImage, new com.squareup.picasso.Callback() {
                        @Override
                        public void onSuccess() {
                            imgProgress.setVisibility(View.GONE);
                            productImage.setVisibility(View.VISIBLE);
                        }

                        @Override
                        public void onError(Exception e) {
                            productImage.setVisibility(View.VISIBLE);

                        }

                    });


        } else {
            productImage.setImageDrawable(mContext.getResources().getDrawable(R.drawable.logo));

        }

        if (!product.getProductName().equalsIgnoreCase("") && product.getProductName() != null && !product.getProductName().equalsIgnoreCase("null")) {
            productName.setText(product.getProductName());
        }
        if (!product.getProductPrice().equalsIgnoreCase("") && product.getProductPrice() != null && !product.getProductPrice().equalsIgnoreCase("null")) {
            productPrice.setText("RS."+product.getProductPrice());
        }
        if (!product.getProductDescription().equalsIgnoreCase("") && product.getProductDescription() != null && !product.getProductDescription().equalsIgnoreCase("null")) {
            productDescription.setText(product.getProductDescription());
        }

*/

    }

    public void viewProductDetails(Product product){

        if (!product.getRatings().equalsIgnoreCase("[]")) {

            ratings = new ArrayList<>();
            try {
                JSONArray jsonArray = new JSONArray(product.getRatings());
                for (int i = 0; i < jsonArray.length(); i++) {
                    usersCount += 1;
                    String ratingCount = jsonArray.getJSONObject(i).getString("rate_count");
                    if(!ratingCount.equalsIgnoreCase("0")) {
                        allRatingCount += Float.valueOf(ratingCount);
                    }
                    Rating rating = new Rating();
                    rating.setRatingId(jsonArray.getJSONObject(i).getString("rating_id"));
                    rating.setProductId(jsonArray.getJSONObject(i).getString("product_id"));
                    rating.setUserId(jsonArray.getJSONObject(i).getString("user_id"));
                    rating.setRateComment(jsonArray.getJSONObject(i).getString("rate_comment"));
                    rating.setRateDateAndTime(jsonArray.getJSONObject(i).getString("date_and_time"));
                    rating.setRateCount(jsonArray.getJSONObject(i).getString("rate_count"));
                    rating.setUserName(jsonArray.getJSONObject(i).getString("userName"));
                    ratings.add(rating);

                }

                LinearLayoutManager layoutManager = new LinearLayoutManager(this);
                ratingListItem.setLayoutManager(layoutManager);

                ratingListAdapter = new RatingListAdapter(mContext, ratings);
                ratingListItem.setAdapter(ratingListAdapter);

                float x = (float) (((double) allRatingCount / (double) Float.valueOf(usersCount)));

                ratingCount.setText(String.valueOf(x));
                ratingUserCount.setText(String.valueOf(usersCount));
                ratingBar.setRating(x);



            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        if (!product.getPromotion().equalsIgnoreCase("[]")) {
            try {
                JSONArray jsonArray = new JSONArray(product.getPromotion());
                for (int i = 0; i < jsonArray.length(); i++) {
                    int promotionStatus = Integer.parseInt(jsonArray.getJSONObject(i).getString("activate_promotion"));

                    if (promotionStatus != 0) {
                        String offerCount = jsonArray.getJSONObject(i).getString("offer_percentage").toString();

                        offer.setVisibility(View.VISIBLE);
                        offer.setText(offerCount + "%");
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }


        if (!product.getProductImage().equalsIgnoreCase("") && product.getProductImage() != null && !product.getProductImage().equalsIgnoreCase("null")) {
            productImage.setVisibility(View.GONE);

            Picasso.get().load(product.getProductImage())
                    .into(productImage, new com.squareup.picasso.Callback() {
                        @Override
                        public void onSuccess() {
                            imgProgress.setVisibility(View.GONE);
                            productImage.setVisibility(View.VISIBLE);
                        }

                        @Override
                        public void onError(Exception e) {
                            productImage.setVisibility(View.VISIBLE);

                        }

                    });


        } else {
            productImage.setImageDrawable(mContext.getResources().getDrawable(R.drawable.logo));
        }

        if (!product.getProductName().equalsIgnoreCase("") && product.getProductName() != null && !product.getProductName().equalsIgnoreCase("null")) {
            productName.setText(product.getProductName());
        }
        if (!product.getProductPrice().equalsIgnoreCase("") && product.getProductPrice() != null && !product.getProductPrice().equalsIgnoreCase("null")) {
            productPrice.setText("RS."+product.getProductPrice());
        }
        if (!product.getProductDescription().equalsIgnoreCase("") && product.getProductDescription() != null && !product.getProductDescription().equalsIgnoreCase("null")) {
            productDescription.setText(product.getProductDescription());
            String[] para = productDescription.getText().toString().split("\r\n\r\n");

        }


    }

}