package com.example.virtualwaitersapp.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DbManager extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "virtual_waiter.db";
    private static final int    DATABASE_VERSION = 1;

    public DbManager(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        try {
            db.execSQL(DbTables.CREATE_TABLE_CATEGORIES);
            db.execSQL(DbTables.CREATE_TABLE_SUBCATEGORIES);
            db.execSQL(DbTables.CREATE_TABLE_PRODUCTS);
            db.execSQL(DbTables.CREATE_TABLE_PROMOTION);

        } catch (Exception e) {
            Log.e(DbManager.class.getSimpleName(), e.getMessage());
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }

}
