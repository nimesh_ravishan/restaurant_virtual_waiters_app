package com.example.virtualwaitersapp.database;

public class DbTables {


    // categories table data fields
    public static final String TABLE_NAME_DISPLAY_CATEGORIES = "categories";

    public static final String ID                         = "_id";
    public static final String CATEGORY_ID                = "category_id";
    public static final String CATEGORY_NAME              = "category_name";
    public static final String CATEGORY_IMAGE             = "category_image";
    public static final String CATEGORY_AVAILABILITY      = "category_availability";

    // categories table create query
    public static final String CREATE_TABLE_CATEGORIES = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME_DISPLAY_CATEGORIES
            + "( " +
            ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
            CATEGORY_ID + " TEXT," +
            CATEGORY_NAME + " TEXT," +
            CATEGORY_IMAGE + " TEXT," +
            CATEGORY_AVAILABILITY + " TEXT" +
            " ); ";


    // subCategories table data fields
    public static final String TABLE_NAME_DISPLAY_SUBCATEGORIES = "sub_categories";

    public static final String SUB_ID                        = "_id";
    public static final String SUBCATEGORY_ID                = "subCategory_id";
    public static final String SUBCATEGORY_CATEGORYID          = "subCategory_category_id";
    public static final String SUBCATEGORY_NAME              = "subCategory_name";
    public static final String SUBCATEGORY_IMAGE             = "subCategory_image";
    public static final String SUBCATEGORY_AVAILABILITY      = "subCategory_availability";

    // subCategories table create query
    public static final String CREATE_TABLE_SUBCATEGORIES = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME_DISPLAY_SUBCATEGORIES
            + "( " +
            SUB_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
            SUBCATEGORY_ID + " TEXT," +
            SUBCATEGORY_CATEGORYID + " TEXT," +
            SUBCATEGORY_NAME + " TEXT," +
            SUBCATEGORY_IMAGE + " TEXT," +
            SUBCATEGORY_AVAILABILITY + " TEXT" +
            " ); ";



    // product table data fields
    public static final String TABLE_NAME_DISPLAY_PRODUCT       = "product";

    public static final String PRO_ID                           = "_id";
    public static final String PRODUCT_ID                       = "product_id";
    public static final String PRODUCT_NAME                     = "product_name";
    public static final String PRODUCT_DESCRIPTION              = "product_description";
    public static final String PRODUCT_PRICE                    = "product_price";
    public static final String PRODUCT_IMAGE                    = "product_image";
    public static final String PRODUCT_CATEGORYID               = "product_category_id";
    public static final String PRODUCT_SUBCATEGORYID            = "product_subcategory_id";
    public static final String PRODUCT_DISCOUNT                 = "product_discount";
    public static final String PRODUCT_PROMOTION                = "product_promotion";
    public static final String PRODUCT_AVAILABILITY             = "product_availability";

    // subCategories table create query
    public static final String CREATE_TABLE_PRODUCTS = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME_DISPLAY_PRODUCT
            + "( " +
            PRO_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
            PRODUCT_ID + " TEXT," +
            PRODUCT_NAME + " TEXT," +
            PRODUCT_DESCRIPTION + " TEXT," +
            PRODUCT_PRICE + " TEXT," +
            PRODUCT_IMAGE + " TEXT," +
            PRODUCT_CATEGORYID + " TEXT," +
            PRODUCT_SUBCATEGORYID + " TEXT," +
            PRODUCT_DISCOUNT + " TEXT," +
            PRODUCT_PROMOTION + " TEXT," +
            PRODUCT_AVAILABILITY + " TEXT" +
            " ); ";


    // promotion table data fields
    public static final String TABLE_NAME_DISPLAY_PROMOTION       = "promotion";

    public static final String PROMO_ID                           = "_id";
    public static final String PROMOTION_ID                       = "promotion_id";
    public static final String PROMOTION_PRODUCT_ID               = "promotion_product_id";
    public static final String PROMOTION_OFFER_PERCENTAGE         = "promotion_offer_percentage";
    public static final String PROMOTION_DESCRIPTION              = "promotion_description";
    public static final String PROMOTION_ACTIVATION               = "promotion_activation";


    // subCategories table create query
    public static final String CREATE_TABLE_PROMOTION = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME_DISPLAY_PROMOTION
            + "( " +
            PROMO_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
            PROMOTION_ID + " TEXT," +
            PROMOTION_PRODUCT_ID + " TEXT," +
            PROMOTION_OFFER_PERCENTAGE + " TEXT," +
            PROMOTION_DESCRIPTION + " TEXT," +
            PROMOTION_ACTIVATION + " TEXT" +
            " ); ";


}
