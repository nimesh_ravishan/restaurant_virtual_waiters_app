package com.example.virtualwaitersapp;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;

import com.example.virtualwaitersapp.Model.Category;
import com.example.virtualwaitersapp.Model.Product;
import com.example.virtualwaitersapp.Model.Promotion;
import com.example.virtualwaitersapp.Model.SubCategory;
import com.example.virtualwaitersapp.Remote.RetrofitClient;
import com.example.virtualwaitersapp.Service.IpService;
import com.example.virtualwaitersapp.Utills.Utills;
import com.example.virtualwaitersapp.database.DBUtils;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SplashActivity extends AppCompatActivity {

    public static final String TAG = "SPLASH SCREEN";
    IpService mService;
    Context mContext;
    ArrayList<SubCategory> subCategoryArrayList;
    ArrayList<Product> productArrayList;
    ArrayList<Promotion> promotionArrayList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        mContext = SplashActivity.this;
        mService = new RetrofitClient().getClient(this).create(IpService.class);

        DBUtils.deleteCategory(mContext);
        DBUtils.deleteSubCategory(mContext);
        DBUtils.deleteProduct(mContext);
        insertCategory();
        insertProduct();

    }

    public void insertProduct() {
        if (!Utills.isNetworkNotAvailable(mContext)) {
            mService.getProducts().enqueue(new Callback<JsonObject>() {
                @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                    if (response.code() == 200) {
                        try {
                            JSONObject jsonObject = new JSONObject(String.valueOf(response.body()));
                            JSONArray jsonArray = jsonObject.getJSONArray("data");
                            productArrayList = new ArrayList<>();

                            for (int x = 0; x < jsonArray.length(); x++) {
                                Product product = new Product();
                                product.setProductId(String.valueOf(jsonArray.getJSONObject(x).get("product_id")));
                                product.setProductName(String.valueOf(jsonArray.getJSONObject(x).get("product_name")));
                                product.setProductDescription(String.valueOf(jsonArray.getJSONObject(x).get("product_description")));
                                product.setProductPrice(String.valueOf(jsonArray.getJSONObject(x).get("product_price")));
                                product.setProductImage(String.valueOf(jsonArray.getJSONObject(x).get("product_image")));
                                product.setProductCategory(String.valueOf(jsonArray.getJSONObject(x).get("product_category")));
                                product.setProductSubCategory(String.valueOf(jsonArray.getJSONObject(x).get("product_sub_category")));
                                product.setProductDiscountPercentage(String.valueOf(jsonArray.getJSONObject(x).get("product_discount_percentage")));
                                product.setPromotion(String.valueOf(jsonArray.getJSONObject(x).get("promotion")));
                                product.setProductAvailability(Integer.valueOf(String.valueOf(jsonArray.getJSONObject(x).get("product_availability"))));
                                productArrayList.add(product);
                            }
                            DBUtils.insertProductsToTable(productArrayList, mContext);


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    } else {
                        Log.e(TAG, "SOMETHING WENT WRONG!");
                    }
                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {

                }
            });
        } else {
            Utills.dialogBox(getString(R.string.network_tittle), getString(R.string.network_message), mContext);
        }
    }

    public void insertPromotion() {
        if (!Utills.isNetworkNotAvailable(mContext)) {
            mService.getPromotion().enqueue(new Callback<JsonObject>() {
                @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                    if (response.code() == 200) {
                        try {
                            JSONObject jsonObject = new JSONObject(String.valueOf(response.body()));
                            JSONArray jsonArray = jsonObject.getJSONArray("data");
                            promotionArrayList = new ArrayList<>();

                            for (int x = 0; x < jsonArray.length(); x++) {
                                Promotion promotion = new Promotion();
                                promotion.setPromotionId(String.valueOf(jsonArray.getJSONObject(x).get("promotion_id")));
                                promotion.setPromoProductId(String.valueOf(jsonArray.getJSONObject(x).get("product_id")));
                                promotion.setOfferPercentage(String.valueOf(jsonArray.getJSONObject(x).get("offer_percentage")));
                                promotion.setPromotionDescription(String.valueOf(jsonArray.getJSONObject(x).get("promotion_description")));
                                promotion.setPromotionActivation(Integer.valueOf(String.valueOf(jsonArray.getJSONObject(x).get("activate_promotion"))));
                                promotionArrayList.add(promotion);
                            }
                            DBUtils.insertPromotionToTable(promotionArrayList, mContext);


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    } else {
                        Log.e(TAG, "SOMETHING WENT WRONG!");
                    }
                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {

                }
            });
        } else {
            Utills.dialogBox(getString(R.string.network_tittle), getString(R.string.network_message), mContext);
        }
    }


    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public void insertCategory() {
        if (!Utills.isNetworkNotAvailable(mContext)) {
            mService.getCategory().enqueue(new Callback<JsonObject>() {
                @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                    if (response.code() == 200) {
                        try {
                            JSONObject jsonObject = new JSONObject(String.valueOf(response.body()));
                            JSONArray jsonArray = jsonObject.getJSONArray("data");

                            ArrayList<Category> categoryArrayList = new ArrayList<>();
                            for (int i = 0; i < jsonArray.length(); i++) {
                                subCategoryArrayList = new ArrayList<>();
                                JSONObject categories = jsonArray.getJSONObject(i);

                                Category category = new Category();
                                category.setCategoryId(categories.getString("category_id"));
                                category.setCategoryName(categories.getString("category_name"));
                                category.setCategoryImage(categories.getString("category_image"));
                                category.setCategoryAvailability(categories.getInt("category_availability"));

                                JSONArray subCategories = categories.getJSONArray("subCategories");
                                if (subCategories.length() != 0) {
                                    for (int x = 0; x < subCategories.length(); x++) {
                                        SubCategory subCategory = new SubCategory();
                                        subCategory.setSubCategoryId(subCategories.getJSONObject(x).getString("sub_category_id"));
                                        subCategory.setCategoryId(subCategories.getJSONObject(x).getString("category_id"));
                                        subCategory.setSubCategoryName(subCategories.getJSONObject(x).getString("sub_category_name"));
                                        subCategory.setSubCategoryImage(subCategories.getJSONObject(x).getString("sub_category_image"));
                                        subCategory.setSubCategoryAvailability(subCategories.getJSONObject(x).getInt("sub_category_availability"));
                                        subCategoryArrayList.add(subCategory);
                                    }
                                    DBUtils.insertSubCategoriesToTable(subCategoryArrayList, mContext);
                                }
                                categoryArrayList.add(category);
                            }
                            DBUtils.insertCategoriesToTable(categoryArrayList, mContext);
                            if (!Utills.getUserIsLogged(mContext)) {
                                Intent login = new Intent(SplashActivity.this, MainActivity.class);
                                startActivity(login);
                                finish();
                            } else {
                                Intent dashboard = new Intent(SplashActivity.this, OrderPlaceActivity.class);
                                startActivity(dashboard);
                                finish();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    } else {
                        Log.e(TAG, "SOMETHING WENT WRONG!");
                    }
                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {

                }
            });
        } else {
            Utills.dialogBox(getString(R.string.network_tittle), getString(R.string.network_message), mContext);
        }
    }
}